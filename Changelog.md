## Changelog

### **[Version 1.0]**

- **EN**
  - The Plug-in has been released.
- **RU** 
  - Релиз плагина


### **[Version 2.0]**

- **EN**
  - Added `/addons/sourcemod/configs/cheating_convars_enabled.ini`
  - Added `/addons/sourcemod/configs/cheating_convars_disabled.ini`
  - Added `/cfg/sourcemod/csgo_ac_cheating_convars.cfg`
  - Added a ConVar **csgo_ac_cheating_convars_ban_time** to allow the game server's owners to decide how long the ban will be, in minutes.
- **RU**
  - Добавлен `/addons/sourcemod/configs/cheating_convars_enabled.ini`
  - Добавлен `/addons/sourcemod/configs/cheating_convars_disabled.ini`
  - Добавлен `/cfg/sourcemod/csgo_ac_cheating_convars.cfg`
  - Добавлена переменная **csgo_ac_cheating_convars_ban_time** что бы дать доступ к установке времени бана владельцам серверов.

### **[Version 2.1]**

- **EN**
  - Added **"cl_crosshair_recoil" "0"** client ConVar to the enabled ConVars file.
  - Added a few seconds delay between the original **OnClientPutInServer** call and the moment of beginning to check the client's ConVars.
- **RU**
  - Добавлена переменная **"cl_crosshair_recoil" "0"** в список проверяемых по умолчанию.
  - Добавлена задержка между **OnClientPutInServer** и началом проверки переменных.

### **[Version 2.2]**

- **EN**
  - Use **MaxClients** instead of **MAXPLAYERS**.
  - Use the client's user index rather than the client's index within the CreateTimer call.
  - Avoid all the fake clients into the game server (BOTs, Replay BOTs & SourceTV BOTs).
- **RU**
  - Используется **MaxClients** вместо **MAXPLAYERS**.
  - Use the client`s user index rather than the client`s index within the CreateTimer call.
  - Не проверяем фейковых клиентов (по типу BOTs, Replay BOTs & SourceTV BOTs).

### **[Version 2.3]**

- **EN**
  - Automatically update "**csgo_ac_cheating_convars**" version ConVar when changing the SMX file and the game server is running.
- **RU**
  - Автоматически обновляем квар "**csgo_ac_cheating_convars**" при смене SMX файла в случае если сервер запущен.

### **[Version 3.0]**

- **EN**
  - Ban players for having different "sv_skyname" value than the game server's.
  - This is a replicated ConVar that exists both client and server side,
  whose value can't be altered by any of the players without a game hack.
- **RU**
  - Бан игроков у которых отличается клиентский квар "sv_skyname" от серверного. Этот квар существует на стороне клиента и сервера, и не может быть изменен без стороннего ПО. 
    - За исключением что могут быть плагины которые его изменяют для клиента отдельно

### **[Version 3.1]**

- **EN**
  - Thanks to [**kratoss1812 (#285749)**](https://forums.alliedmods.net/member.php?u=285749) @ [**/showpost.php?p=2744238&postcount=23**](https://forums.alliedmods.net/showpost.php?p=2744238&postcount=23).
  - Replaced `CreateArray(PLATFORM_MAX_PATH)` with `CreateArray(ByteCountToCells(PLATFORM_MAX_PATH))`.
  - Added a definition `#define CHECK_SV_SKYNAME_TOO` which everyone is able to undefine or delete
- **RU**
  - Спасибо [**kratoss1812 (#285749)**](https://forums.alliedmods.net/member.php?u=285749) [**/showpost.php?p=2744238&postcount=23**](https://forums.alliedmods.net/showpost.php?p=2744238&postcount=23).
  - Заменено `CreateArray(PLATFORM_MAX_PATH)` на `CreateArray(ByteCountToCells(PLATFORM_MAX_PATH))`
  - Добавлено определение `#define CHECK_SV_SKYNAME_TOO` для тех кто хочет его удалить
    - Это нужно **удалить / закомментировать** если вы используете плагины которые изменяют **skybox**
