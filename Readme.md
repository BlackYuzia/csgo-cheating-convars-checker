# CS:GO Anti-Cheat: Cheating ConVars

[![Plugin version 3.1](https://img.shields.io/badge/Version-3.1-brightgreen)](https://gitlab.com/BlackYuzia/csgo-cheating-convars-checker/-/releases)
[![Support RU](https://img.shields.io/badge/Support-RU-brightgreen)](https://hlmod.ru/threads/cheat-convars-checker.53449/)
[![Support EN](https://img.shields.io/badge/Support-EN-brightgreen)](https://forums.alliedmods.net/showthread.php?t=324601)

### **Links**

- [**Alliedmods Topic**](https://forums.alliedmods.net/showthread.php?t=324601)
- [**Author**](https://forums.alliedmods.net/member.php?u=69445)
- [**HLMOD Topic**](https://hlmod.ru/resources/cheat-convars-checker.1800/)
- [**Changelog**](Changelog.md)